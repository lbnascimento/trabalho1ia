import controller_template as controller_template
import random
import logging
import math

class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)

    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """

        features = self.compute_features(self.sensors)
        acao = [0 for i in range(5)]

        for k, i in zip(range(5), range(0, 15, 3)):
            acao[k] = parameters[i] + parameters[i + 1] * features[0] + parameters[i + 2] * features[1]

        return acao.index(max(acao)) + 1

    def compute_features(self, sensors):
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                      
            track_distance_center: 1-100                    
            track_distance_right: 1-100                     
            on_track: 0 or 1                                
            checkpoint_distance: 0-???                      
            car_velocity: 10-200                            
            enemy_distance: -1 or 0-???                     
            position_angle: -180 to 180                     
            enemy_detected: 0 or 1  
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """
        sensors = list(map(float, sensors))
        feature1 = sensors[1] - sensors[2]
        feature2 = sensors[1] - sensors[0]

        feature1 = ((feature1 + 100) / 200) * 2 - 1
        feature2 = ((feature2 + 100) / 200) * 2 - 1

        return [feature1,feature2]

    @staticmethod
    def shouldAccept(prob):
        return random.random()<prob

    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        value = self.run_episode(weights)
        solucao_atual = weights[:]
        iteracoes = 50
        T = 1000
        N = 10

        logging.basicConfig(filename='valor_simulated.log', level=logging.INFO, filemode='a')

        while True:
            T = 0.7*T

            print(T," temp - i",iteracoes)
            message = "valor da função objetivo {} int {}".format(value, 50 - iteracoes)
            logging.info(message)

            iteracoes-=1
            if T == 0 or iteracoes == 0:
                return solucao_atual

            for i in range(N):
                solucao_candidata = list(map(lambda x: x + random.uniform(-7, 7), solucao_atual))
                value_candidata = self.run_episode(solucao_candidata)

                delta =  value_candidata - value

                if(delta > 0):
                    solucao_atual = solucao_candidata
                    value = value_candidata
                else:
                    solucao_atual = solucao_candidata if Controller.shouldAccept(math.exp(delta/T)) else solucao_atual

