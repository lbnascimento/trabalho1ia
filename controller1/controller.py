import controller_template as controller_template
import random
import logging


class Controller(controller_template.Controller):
    def __init__(self, track, evaluate=True, bot_type=None):
        super().__init__(track, evaluate=evaluate, bot_type=bot_type)

    def take_action(self, parameters: list) -> int:
        """  
        :param parameters: Current weights/parameters of your controller
        :return: An integer corresponding to an action:
        1 - Right 
        2 - Left
        3 - Accelerate 
        4 - Brake
        5 - Nothing
        """
        
        features = self.compute_features(self.sensors)
        acao = [0 for i in range(5)]
        #print(len(parameters))

        for k, i in zip(range(5), range(0,15,3)):
            acao[k] = parameters[i] + parameters[i + 1] * features[0] + parameters[i + 2] * features[1]

        return acao.index(max(acao))+1

    def compute_features(self, sensors):
        """        
        :param sensors: Car sensors at the current state s_t of the race/game
        contains (in order):        
            track_distance_left: 1-100                      
            track_distance_center: 1-100                    
            track_distance_right: 1-100                     
            on_track: 0 or 1                                
            checkpoint_distance: 0-???                      
            car_velocity: 10-200                            
            enemy_distance: -1 or 0-???                     
            position_angle: -180 to 180                     
            enemy_detected: 0 or 1  
          (see the specification file/manual for more details)
        :return: A list containing the features you defined 
        """
        sensors = list(map(float, sensors))
        feature1 = sensors[1] - sensors[2]
        feature2 = sensors[1] - sensors[0]


        feature1 = ((feature1 + 100) / 200) * 2 - 1
        feature2 = ((feature2 + 100) / 200) * 2 - 1

        return [feature1,feature2]

    def sum_random(self, number):
        return number + random.uniform(-0.5, 5)

        
    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE
        
        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        logging.basicConfig(filename='valor_hill_climbing.log', level=logging.INFO, filemode='a')



        encontrou_melhor = True
        father = self.run_episode(weights)
        results = [0 for i in range(100)]
        var = [0 for i in range(100)]
        z = 0;

        while encontrou_melhor:

            message = "valor da função objetivo {} int {}".format(father,z)
            z+=1
            logging.info(message)

            encontrou_melhor = False
            for i in range(100):
                var[i] = weights[:]
                var[i] = list(map(self.sum_random,var[i]))
                results[i] = self.run_episode(var[i])
                max_ind = results.index(max(results))
                #print(max_ind)
                if(results[max_ind]>father):
                    father = results[max_ind]
                    weights = var[max_ind]
                    encontrou_melhor = True

        return weights